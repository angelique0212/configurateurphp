<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

    <?php

    // $x affiche un chiffre entre 0 et 2
    // Si $x = 0, afficher $x est égal 0, si $x = 1, affichez $x est égal à 1, si $x = 2 affichez $x est égal à 2
    // Utilisez switch
    
    ?>
    
    <!-- écrire le code après ce commentaire -->
    <?php
    
    $x = rand(0,2);
    switch ($x) {
        case 0:
            echo " égal 0";
            break;
        case 1:
            echo " égal 1";
            break;
        case 2:
            echo " égal 2";
            break;
    }
    
    ?>
    <!-- écrire le code avant ce commentaire -->

</body>
</html>