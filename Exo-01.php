<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    
    
    <!-- écrire le code après ce commentaire -->
    
    <?php
    
    // A l'aide de la fonction echo, affichez : Ma voiture est un Zafira de la marque Opel
    
    $marque = "Opel";
    $modele = "Zafira";

    echo "ma voiture est un ". $modele. " de la marque ". $marque.'<br>';
    
    ?>
    <!-- écrire le code avant ce commentaire -->

</body>
</html>