<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

    <?php

    // Avec la boucle foreach, affichez les clés et les valeurs du tableau $table
    
    ?>
    
    <!-- écrire le code après ce commentaire -->
    <?php
    
    $table = ['Banane' => 'Jaune', 'Pomme' => 'Verte', 'Clémentine' => 'Orange'];
    foreach($table as $fruits=>$color){
        echo $fruits, $color;
    }
    
    ?>
    <!-- écrire le code avant ce commentaire -->

</body>
</html>