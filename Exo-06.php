<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

    <?php

    // Formater la suite numérique en separant les milliers par des espaces
    // https://www.php.net/manual/fr/function.number-format
    
    ?>
    
    <!-- écrire le code après ce commentaire -->
    <?php
    
    $total = 123456;
    echo  number_format($total, 0,' ', ' ');
    
    
    ?>
    <!-- écrire le code avant ce commentaire -->

</body>
</html>